# InAppBrowser fix

![alt text](https://gitlab.com/norpoint/inappbrowser-fix/-/raw/master/demo.png "Demo")

## Problem
Some sites like pensionsinfo.dk use a CSP header to secure against XSS attacks. This disables support for some language primities like eval. 
InAppBrowser uses eval when a value must be returned from an injected script.


## PostMessage approach

To avoid InAppBrowsers eval when returning a value, post-messaging is used instead. InAppBrowser will tunnel the message from site back to cordova app.  

```javascript
function onDeviceReady() {
    // Cordova is now initialized. Have fun!
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
    var iab = cordova.InAppBrowser.open('https://pensionsinfo.dk/Welcome');
    iab.addEventListener('message', event => {
        console.log('RECEIVED', event.data.message);
    });
    iab.addEventListener('loadstop', function() {
        console.log('LOADSTOP');
        iab.executeScript({code: "webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({ message: location.href }))" });
    });
}
```

## To install
Setup cordova and Android Studio. Then open/import /platform/android from Android Studio.

Alternatively try
```javascript
cordova build
cordova run
```
